Problemática.

La empresa D’ROCHA S.A dedicada a la industria farmacéutica en donde se fabrican productos para controlar la proliferación del T-Virus fabricado por UMBRELLA CORPS, desea Implementar el Balanced Scorecard (BSC) como herramienta estratégica para la consolidación de objetivos en cierto tiempo; El BSC permitirá implementar la estrategia y la misión a partir de un conjunto de medidas de decisión para transformarlas en acción. La empresa requiere una aplicación web para garantizar que todos los involucrados en la toma de decisiones participen del proceso estratégico.

Un equipo de ingenieros funcionales adelanto el trabajo de realizar un análisis de la problemática, entrevistar a cada uno de los involucrados dentro de la empresa, encontrando algunas necesidades que se describen a continuación junto con información acerca de la herramienta requerida:

¿Qué es el BSC?: El Balanced Scorecard (BSC / Cuadro de Mando Integral) es una herramienta que permite enlazar estrategias y objetivos clave con desempeño y resultados a través de cuatro áreas críticas en cualquier empresa: desempeño financiero, conocimiento del cliente, procesos internos de negocio y aprendizaje y crecimiento.

Las restricciones que encontró el área funcional son:
- Debe existir la siguiente relación: Un objetivo debe tener al menos un indicador, una meta y una iniciativa para poder ser visualizado en el cuadro integral de mando de la aplicación web.
- Cada uno de los involucrados de las áreas asignadas puede generar el CRUD completo de los objetivos, indicadores, metas e iniciativas de su perspectiva, pero podrá visualizar las demás perspectivas sin poder realizar acción alguna.
- Las áreas de la organización decidieron que solamente se podrán definir un máximo de 10 objetivos para cada una de las perspectivas.

El área funcional desarrollo un prototipo de la aplicación web el cual puede encontrar con el nombre Anexo 1 - Modelo BSC.