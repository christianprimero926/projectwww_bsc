<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Christian Primero',
            'email' => 'admin@correo.com',            
            'password' => Hash::make('secret'),
            'role_id' => 1
        ]);
    }
}
