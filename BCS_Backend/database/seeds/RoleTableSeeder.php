<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'name' => 'Administrador'        	
        ]);
        Role::create([
        	'name' => 'Financiero'        	
        ]);
        Role::create([
        	'name' => 'Servicio al Cliente'        	
        ]);
        Role::create([
        	'name' => 'Gestión Humana'        	
        ]);
        Role::create([
        	'name' => 'I+D'        	
        ]);

    }
}
