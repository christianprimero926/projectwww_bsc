<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Indicator extends Model
{
    //
    use SoftDeletes;

    protected $fillable = ['name', 'objective_id'];

    //Relationships
    public function objective()
    {
        return $this->belongsTo('App\Objective');
    }
}
