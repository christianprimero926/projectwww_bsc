<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Objective extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'user_id', 'role_id'];
    
    //Relationships
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function initiative()
    {
    	return $this->hasMany('App\Initiative');
    }
    public function indicator()
    {
    	return $this->hasMany('App\Indicator');
    }
    public function goal()
    {
    	return $this->hasMany('App\Goal');
    }
}
