<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'user_id'];
    
    //Relationships
    public function user()
    {
    	return $this->hasMany('App\User');
    }

    public function objective()
    {
    	return $this->hasMany('App\Objective');
    }

}
