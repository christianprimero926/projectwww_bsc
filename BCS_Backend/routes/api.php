<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'Auth\LoginController@login');
Route::post('auth/refresh', 'Auth\LoginController@refresh');
Route::get('auth/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Auth\\'], function() {
    Route::get('auth/authenticated', 'LoginController@authenticatedUser');
});

Route::group(['prefix' => 'managmentUser', 'middleware' => 'cors'], function(){
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
});

Route::group(['prefix' => 'strategies', 'middleware' => 'cors'], function(){
    Route::resource('objectives', 'ObjectiveController');
    Route::resource('indicators', 'IndicatorController');
    Route::resource('goals', 'GoalController');
    Route::resource('initiatives', 'InitiativeController');
});

