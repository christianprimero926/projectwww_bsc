import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';

import { AdminComponent } from './../admin.component';
import { AdminDashboard2Component } from './../admin-dashboard2/admin-dashboard2.component';
import { AdminDashboard1Component } from './../admin-dashboard1/admin-dashboard1.component';
import { ProfileComponent } from '../../auth/profile/profile.component';
import { ClientsComponent } from '../../views/clients/clients.component';
import { FinancialComponent } from '../../views/financial/financial.component';
import { LearningComponent } from '../../views/learning/learning.component';
import { InternalComponent } from '../../views/internal/internal.component';
import { PermitsComponent } from '../admin-panel/permits/permits.component';
import { UsersComponent } from '../admin-panel/users/users.component';
import { ProfilesComponent } from '../admin-panel/profiles/profiles.component';
import { CreateUserComponent } from '../admin-panel/create-user/create-user.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'admin',
        component: AdminComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
        children: [
          {
            path: '',
            redirectTo: 'dashboard1',
            pathMatch: 'full'
          },
          { path: 'dashboard1', component: AdminDashboard1Component },
          { path: 'dashboard2', component: AdminDashboard2Component },
          { path: 'profile', component: ProfileComponent },
          { path: 'clients', component: ClientsComponent },
          { path: 'financial', component: FinancialComponent },
          { path: 'learning', component: LearningComponent },
          { path: 'internal', component: InternalComponent },
          { path: 'users', component: UsersComponent },
          { path: 'createU', component: CreateUserComponent},
          { path: 'permits', component: PermitsComponent },
          { path: 'profiles', component: ProfilesComponent },
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
