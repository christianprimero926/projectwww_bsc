import { DataTablesModule } from 'angular-datatables';
import { UsersService } from './admin-panel/services/users.service';
import { AdminRoutingModule } from './admin-routing/admin-routing.module';
import { AdminDashboard1Component } from './admin-dashboard1/admin-dashboard1.component';
import { AdminControlSidebarComponent } from './admin-control-sidebar/admin-control-sidebar.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminContentComponent } from './admin-content/admin-content.component';
import { AdminLeftSideComponent } from './admin-left-side/admin-left-side.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminDashboard2Component } from './admin-dashboard2/admin-dashboard2.component';
import { ProfilesComponent } from './admin-panel/profiles/profiles.component';
import { UsersComponent } from './admin-panel/users/users.component';
import { PermitsComponent } from './admin-panel/permits/permits.component';
import { FormsModule } from '@angular/forms';
import { CreateUserComponent } from './admin-panel/create-user/create-user.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    DataTablesModule
  ],
  declarations: [
    AdminComponent,
    AdminHeaderComponent,
    AdminLeftSideComponent,
    AdminContentComponent,
    AdminFooterComponent,
    AdminControlSidebarComponent,
    AdminDashboard1Component,
    AdminDashboard2Component,
    ProfilesComponent,
    UsersComponent,
    PermitsComponent,
    CreateUserComponent

  ],
  exports: [AdminComponent],
  providers: [
    UsersService
  ]
})
export class AdminModule { }
