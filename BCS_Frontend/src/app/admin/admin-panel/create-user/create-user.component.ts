import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from './../../../interfaces/user.model';
import { OnInit, Component } from '@angular/core';
declare var AdminLTE: any;
declare var $;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  user: User;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.http.get<any>(`${environment.api_url}/auth/authenticated`).subscribe(data => {
      this.user = data.user;
      // console.log(this.user);
    });
    // Update the AdminLTE layouts
    AdminLTE.init();
    // Make the dashboard widgets sortable Using jquery UI
    jQuery('.connectedSortable').sortable({
      placeholder: 'sort-highlight',
      connectWith: '.connectedSortable',
      handle: '.box-header, .nav-tabs',
      forcePlaceholderSize: true,
      zIndex: 999999
    });
    jQuery('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

    // jQuery UI sortable for the todo list
    jQuery('.todo-list').sortable({
      placeholder: 'sort-highlight',
      handle: '.handle',
      forcePlaceholderSize: true,
      zIndex: 999999
    });
  }
  addUser() {
    console.log(this.user);
  }
}
