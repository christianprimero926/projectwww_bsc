
import { Component, OnInit } from '@angular/core';
import { User } from '../../../interfaces/user.model';
import { AuthService } from '../../../auth/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { } from 'jquery';
import { } from 'jquery.slimscroll';
import { Role } from '../../../interfaces/role.model';
// Variable in assets/js/scripts.js file
declare var AdminLTE: any;
declare var $;
class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  API_ENDPOINT = 'http://localhost:8000/api';
  user: User;

  dtOptions: DataTables.Settings = {};
  users: User[];

  constructor(private auth: AuthService, private http: HttpClient) {
    http.get(this.API_ENDPOINT + '/managmentUser/user').subscribe((data: User[]) => {
      this.users = data;
      // this.users = data;
      // console.log(data);
    });
  }

  ngOnInit() {
    this.http.get<any>(`${environment.api_url}/auth/authenticated`).subscribe(data => {
      this.user = data.user;
      // console.log(this.user);
    });
     // Update the AdminLTE layouts
     AdminLTE.init();
     // Make the dashboard widgets sortable Using jquery UI
     jQuery('.connectedSortable').sortable({
       placeholder: 'sort-highlight',
       connectWith: '.connectedSortable',
       handle: '.box-header, .nav-tabs',
       forcePlaceholderSize: true,
       zIndex: 999999
     });
     jQuery('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

     // jQuery UI sortable for the todo list
     jQuery('.todo-list').sortable({
       placeholder: 'sort-highlight',
       handle: '.handle',
       forcePlaceholderSize: true,
       zIndex: 999999
     });
     const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http
          .post<DataTablesResponse>(
            this.API_ENDPOINT + '/managmentUser/user',
            dataTablesParameters, {}
          ).subscribe(resp => {
            that.users = resp.data;

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
      columns: [{ data: 'email' }, { data: 'name' }, { data: 'role_id' }]
    };
  }
}
