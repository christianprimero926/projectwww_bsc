import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponseBase, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';


import { User } from '../../interfaces/user.model';

@Injectable()
export class AuthService {
  private users: User[] = [];
  API_ENDPOINT = 'http://localhost:8000/api';
  constructor(private http: HttpClient, private router: Router) { }
/*
  save(user: Object): Observable<User[]> {
    return this.http.post(this.API_ENDPOINT + '/managmentUser/user', user)
    .map((response: Response) => response.json())
    .catch((error: any) => Observable.throw('Error in x service'));
    /* const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(this.API_ENDPOINT + '/managmentUser/user', user, {headers: headers}); */
/*}*/

  save(user: User) {
    this.users.push(user);
    const body = JSON.stringify(user);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(this.API_ENDPOINT + '/managmentUser/user', body, { headers: headers})
        .map((response: HttpResponseBase) => response)
        .catch((error: HttpErrorResponse ) => Observable.throw(error));
  }

  check(): boolean {
    return localStorage.getItem('user') ? true : false;
  }

  login(credentials: {email: string, password: string}): Observable<boolean> {
    return this.http.post<any>(`${environment.api_url}/auth/login`, credentials)
    .do(data => {
      localStorage.setItem('token', data.token);
      localStorage.setItem('user', btoa(JSON.stringify(data.user)));
    });
  }

  logout(): void {
    this.http.get(`${environment.api_url}/auth/logout`).subscribe(resp => {
      console.log(resp);
      localStorage.clear();
      this.router.navigate(['auth/login']);
    });
  }

  getUser(): User {
    return localStorage.getItem('user') ? JSON.parse(atob(localStorage.getItem('user'))) : null;
  }

  setUser(): Promise<boolean> {
    return this.http.get<any>(`${environment.api_url}/auth/authenticatedUser`).toPromise()
    .then(data => {
      if (data.user) {
        localStorage.setItem('user', btoa(JSON.stringify(data.user)));
        return true;
      }
      return false;
    });
  }
}
