import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from '../../interfaces/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(private auth: AuthService, private http: HttpClient) { }

  ngOnInit() {
    this.http.get<any>(`${environment.api_url}/auth/authenticated`).subscribe(data => {
      this.user = data.user;
    });
  }

}
