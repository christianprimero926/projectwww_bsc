import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  f: FormGroup;
  errorCredentials = false;
  bodyClasses = 'hold-transition login-page';
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {

    // add the the body classes
    this.body.classList.add('hold-transition');
    this.body.classList.add('login-page');

    this.f = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });

    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    // remove the the body classes
    this.body.classList.remove('hold-transition');
    this.body.classList.remove('login-page');
  }


  onSubmit() {
    this.authService.login(this.f.value).subscribe(
      (resp) => {
        this.router.navigate(['admin']);
      },
      (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse);
        if (errorResponse.status === 401) {
          this.errorCredentials = true;
        }
      }
    );
  }
}
