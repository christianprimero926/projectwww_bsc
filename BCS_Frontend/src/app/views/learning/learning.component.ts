import { Component, OnInit } from '@angular/core';
import { } from 'jquery';
import { } from 'jquery.slimscroll';
import { User } from '../../interfaces/user.model';
import { AuthService } from '../../auth/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
// Variable in assets/js/scripts.js file
declare var AdminLTE: any;
declare var $;

@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.css']
})
export class LearningComponent implements OnInit {
  user: User;

  constructor(private auth: AuthService, private http: HttpClient) { }

  ngOnInit() {
    this.http.get<any>(`${environment.api_url}/auth/authenticated`).subscribe(data => {
      this.user = data.user;
    });
     // Update the AdminLTE layouts
     AdminLTE.init();
     // Make the dashboard widgets sortable Using jquery UI
     jQuery('.connectedSortable').sortable({
       placeholder: 'sort-highlight',
       connectWith: '.connectedSortable',
       handle: '.box-header, .nav-tabs',
       forcePlaceholderSize: true,
       zIndex: 999999
     });
     jQuery('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

     // jQuery UI sortable for the todo list
     jQuery('.todo-list').sortable({
       placeholder: 'sort-highlight',
       handle: '.handle',
       forcePlaceholderSize: true,
       zIndex: 999999
     });

     $(() => {
      var scntInd = $('#newIndicator');
      $(document).on('click', '#addIndicator', function () {
        $('<b>'+
            '<p class="col-sm-2"></p>'+
            '<p  class="input-group col-sm-9">'+
              '<input id="inputeste" type="text" class="form-control" placeholder="Escribe un nuevo indicador..."> '+
              '<span class="input-group-btn">'+
                '<a class="btn btn-danger" href="javascript:void(0)" id="remIndicator">'+
                  '<i class="fa fa-minus-circle"></i>'+
                '</a>'+
              '</span>'+
            '</p>'+
          '</b>').appendTo(scntInd);
        return false;
      });
      $(document).on('click', '#remIndicator', function () {
            $(this).parents('b').remove();
          return false;
      });

      var scntGoal = $('#newGoal');
      $(document).on('click', '#newGoal', function () {
        $('<b>'+
            '<p class="col-sm-2"></p>'+
            '<p  class="input-group col-sm-9">'+
              '<input id="inputeste" type="text" class="form-control" placeholder="Escribe una nueva meta..."> '+
              '<span class="input-group-btn">'+
                '<a class="btn btn-danger" href="javascript:void(0)" id="remGoal">'+
                  '<i class="fa fa-minus-circle"></i>'+
                '</a>'+
              '</span>'+
            '</p>'+
          '</b>').appendTo(scntGoal);
        return false;
      });
      $(document).on('click', '#remGoal', function () {
            $(this).parents('b').remove();
          return false;
      });

      var scntIni = $('#newInitiative');
      $(document).on('click', '#addInitiative', function () {
        $('<b>'+
            '<p class="col-sm-2"></p>'+
            '<p  class="input-group col-sm-9">'+
              '<input id="inputeste" type="text" class="form-control" placeholder="Escribe una nueva iniciativa..."> '+
              '<span class="input-group-btn">'+
                '<a class="btn btn-danger" href="javascript:void(0)" id="remInitiative">'+
                  '<i class="fa fa-minus-circle"></i>'+
                '</a>'+
              '</span>'+
            '</p>'+
          '</b>').appendTo(scntIni);
        return false;
      });
      $(document).on('click', '#remInitiative', function () {
            $(this).parents('b').remove();
          return false;
      });
      $('#example1').DataTable()
      $('#example').DataTable({
        'paging'      : false,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
      });
    });
  }
}
