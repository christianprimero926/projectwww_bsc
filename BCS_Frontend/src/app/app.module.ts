import { DataTablesModule } from 'angular-datatables';
import { UsersService } from './admin/admin-panel/services/users.service';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AplicationErrorHandler } from './app.error-handler';
import { RefreshTokenInterceptor } from './interceptors/refresh-token.interceptor';


import { AuthModule } from './auth/auth.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './guards/auth.guard';
import { ClientsComponent } from './views/clients/clients.component';
import { FinancialComponent } from './views/financial/financial.component';
import { InternalComponent } from './views/internal/internal.component';
import { LearningComponent } from './views/learning/learning.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ClientsComponent,
    FinancialComponent,
    InternalComponent,
    LearningComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    AdminModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true },
    { provide: ErrorHandler, useClass: AplicationErrorHandler },
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
