export interface Initiative {
  id?: number;
  name: string;
  objective_id: number;
}
