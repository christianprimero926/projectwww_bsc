export interface Goal {
  id?: number;
  name: string;
  objective_id: number;
}
