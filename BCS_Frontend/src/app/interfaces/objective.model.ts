export interface Objective {
  id?: number;
  name: string;
  description: string;
  user_id: number;
  role_id: number;
}
