export interface Indicator {
  id?: number;
  name: string;
  objective_id: number;
}
